package com.example.graphsample.controller;

public interface IController {

    void update();

    void clear();

    void release();

    void switchToDfsMode();

}
