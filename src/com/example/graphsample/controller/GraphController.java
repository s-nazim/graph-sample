package com.example.graphsample.controller;

import android.view.MotionEvent;

import com.example.graphsample.logging.Logging;
import com.example.graphsample.model.GraphModel;
import com.example.graphsample.model.IModel;
import com.example.graphsample.model.ShowMode;
import com.example.graphsample.render.IRender;
import com.example.graphsample.render.OnTouchListener;

public class GraphController implements IController {

    private int mStartX = 0;
    private int mStartY = 0;
    private IRender mRender;
    private IModel mGraphModel;

    public GraphController(IRender render) {
        mRender = render;
        mGraphModel = new GraphModel(this);
        mRender.setModel(mGraphModel);
        mRender.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(MotionEvent event) {
                switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    mStartX = (int) event.getX();
                    mStartY = (int) event.getY();
                    Logging.w("ACTION_DOWN ");
                    return true;
                case MotionEvent.ACTION_UP:
                    Logging.w("ACTION_UP ");
                    if (Math.abs(event.getX() - mStartX) < 20 || Math.abs(event.getY() - mStartY) < 20) {
                        mGraphModel.onTouch(event.getX(), event.getY());
                    } else {
                        mGraphModel.onMove(mStartX, mStartY, event.getX(), event.getY());
                    }
                    return true;
                default:
                    return false;
                }
            }
        });
    }

    @Override
    public void update() {
        mRender.update();
    }

    @Override
    public void clear() {
        mGraphModel.clear();
        update();
    }

    @Override
    public void release() {
        mRender.release();
    }

    @Override
    public void switchToDfsMode() {
        mGraphModel.switchMode(ShowMode.DFS);
    }

}
