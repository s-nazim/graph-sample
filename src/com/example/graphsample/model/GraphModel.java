package com.example.graphsample.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.graphics.Color;
import android.os.SystemClock;

import com.example.graphsample.controller.IController;
import com.example.graphsample.graph.Edge;
import com.example.graphsample.graph.IEntity;
import com.example.graphsample.graph.Vertex;
import com.example.graphsample.logging.Logging;

public class GraphModel implements IModel {

    private List<Vertex> mListVertex = new ArrayList<Vertex>();
    private List<Edge> mListEdge = new ArrayList<Edge>();
    private IController mController;

    public GraphModel(IController controller) {
        mController = controller;
    }

    @Override
    public void onTouch(float x, float y) {
        Vertex vertex = new Vertex(x, y, 50, mListVertex.size());
        if (isExistIntersection(x, y)) {
            return;
        }
        Logging.w(" onTouch() " + vertex.number);
        mListVertex.add(vertex);
        mController.update();
    }

    private boolean isExistIntersection(float x, float y) {
        for (Vertex v : mListVertex) {
            float dx = x - v.x;
            float dy = y - v.y;
            if (Math.sqrt(dx * dx + dy * dy) <= v.radius * 2) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onMove(int startX, int startY, float endX, float endY) {
        Logging.w("onMove() ");
        LinkedList<Vertex> tempQueue = new LinkedList<Vertex>();
        for (Vertex v : mListVertex) {
            // Need rewrite
            float dx = startX - v.x;
            float dy = startY - v.y;
            if (Math.sqrt(dx * dx + dy * dy) <= v.radius + v.radius) {
                Vertex last = tempQueue.pollLast();
                if (last != null) {
                    tempQueue.add(v);
                    tempQueue.add(last);
                    continue;
                }
                tempQueue.add(v);
            }
            dx = endX - v.x;
            dy = endY - v.y; 
            if (Math.sqrt(dx * dx + dy * dy) <= v.radius + v.radius) {
                tempQueue.add(v);
            }
        }
        if (tempQueue.size() > 1) {
            Logging.w("onMove() edge was added " + tempQueue.get(0).number + " --> " + tempQueue.get(1).number);
            mListEdge.add(new Edge(tempQueue.get(0), tempQueue.get(1)));
            mController.update();
        }
    }

    @Override
    public void clear() {
        mListVertex.clear();
        mListEdge.clear();
    }

    @Override
    public List<IEntity> getEntities() {
        List<IEntity> entities = new ArrayList<IEntity>();
        for (int i = 0; i < mListEdge.size(); i++) {
            entities.add(mListEdge.get(i));
        }
        for (int i = 0; i < mListVertex.size(); i++) {
            entities.add(mListVertex.get(i));
        }
        return entities;
    }

    @Override
    public void switchMode(ShowMode mode) {
        ArrayList<Integer>[] listVetex = new ArrayList[mListVertex.size()];
        for (int i = 0; i < mListVertex.size(); i++) {
            listVetex[i] = new ArrayList<Integer>();
            Vertex vertex = mListVertex.get(i);
            for (int j = 0; j < mListEdge.size(); j++) {
                Edge edge = mListEdge.get(j);
                if (edge.vertexFirst.equals(vertex)) {
                    listVetex[i].add(edge.vertexSecond.number);
                } else if (edge.vertexSecond.equals(vertex)) {
                    listVetex[i].add(edge.vertexFirst.number);
                }
                Logging.w("edge = " + j);
            }
        }
        mode.run(listVetex, mListVertex, new ShowMode.OnVisitListener() {

            @Override
            public void onVisited(Vertex vertex) {
                vertex.setColor(Color.RED);
                SystemClock.sleep(300); // Need re-think
                mController.update();
            }
        });
        Logging.w("switchMode() " + mode);
    }

}
