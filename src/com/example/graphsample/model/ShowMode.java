package com.example.graphsample.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.locks.ReentrantLock;

import com.example.graphsample.graph.Vertex;

public enum ShowMode {
    DFS {
        private final ReentrantLock lock = new ReentrantLock();

        @Override
        void calculate(ArrayList<Integer>[] listVetex, List<Vertex> mListVertex, OnVisitListener onVisitListener) {
            lock.lock();
            Stack<Integer> stack = new Stack<Integer>();
            boolean[] visited = new boolean[mListVertex.size()];
            stack.push(0);
            while(!stack.isEmpty()) {
                int index = stack.pop();
                visited[index] = true;
                onVisitListener.onVisited(mListVertex.get(index));
                for (int i = 0; i < listVetex[index].size(); i++) {
                    if (visited[listVetex[index].get(i)]) {
                        continue;
                    }
                    stack.push(listVetex[index].get(i));
                    break;
                }
                if (stack.isEmpty()) {
                    for (int i = 0; i < visited.length; i++) {
                        if (!visited[i]) {
                            stack.push(i);
                            break;
                        }
                    }
                }
            }
        }
    },
    BFS {
        @Override
        void calculate(ArrayList<Integer>[] listVetex, List<Vertex> mListVertex, OnVisitListener onVisitListener) {
            // TODO Auto-generated method stub
        }
    };

    interface OnVisitListener {
        void onVisited(Vertex vertex);
    }

    abstract void calculate(ArrayList<Integer>[] listVetex, List<Vertex> mListVertex, OnVisitListener onVisitListener);

    public void run(ArrayList<Integer>[] listVetex, List<Vertex> mListVertex, OnVisitListener onVisitListener) {
        calculate(listVetex, mListVertex, onVisitListener);
    }
}
