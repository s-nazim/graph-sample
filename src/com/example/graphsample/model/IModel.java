package com.example.graphsample.model;

import java.util.List;

import com.example.graphsample.graph.IEntity;

public interface IModel {

    void onTouch(float x, float y);

    void clear();

    void onMove(int startX, int startY, float endX, float endY);

    List<IEntity> getEntities();

    void switchMode(ShowMode mode);

}
