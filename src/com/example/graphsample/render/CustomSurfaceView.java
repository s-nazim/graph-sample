package com.example.graphsample.render;

import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.graphsample.graph.IEntity;
import com.example.graphsample.logging.Logging;
import com.example.graphsample.model.IModel;

public class CustomSurfaceView extends SurfaceView implements SurfaceHolder.Callback, IRender {

    private RenderThread mRenderThread;
    private com.example.graphsample.render.OnTouchListener mTouchListener;
    private IRenderLifeCycleListener mLifeCycleListener;
    private IModel mGraphModel;

    public CustomSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
    }

    public CustomSurfaceView(Context context) {
        super(context);
        getHolder().addCallback(this);
    }

    public CustomSurfaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        getHolder().addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (mLifeCycleListener != null) {
            mLifeCycleListener.onSurfaceCreated();
        }
        mRenderThread = new RenderThread(holder, mGraphModel);
        mRenderThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // TODO
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mLifeCycleListener != null) {
            mLifeCycleListener.onSurfaceDestroyed();
        }
        Logging.w("CustomSurfaceView :: surfaceDestroyed()");
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mTouchListener != null) {
            return mTouchListener.onTouch(event);
        }
        return super.onTouchEvent(event);
    }

    class RenderThread extends Thread {
        private SurfaceHolder mHolder;
        private boolean isRunning;
        private boolean isNeedRedraw;
        private IModel mGraphModel;

        public RenderThread(SurfaceHolder holder, IModel graphModel) {
            mHolder = holder;
            mGraphModel = graphModel;
        }

        @Override
        public void run() {
            while (isRunning && !isInterrupted()) {
                Canvas canvas = null;
                if (mHolder.getSurface().isValid() && isNeedRedraw) {
                    try {
                        canvas = mHolder.lockCanvas(null);
                        canvas.drawColor(Color.TRANSPARENT, Mode.CLEAR);
                        Logging.w("RenderThread :: run() canvas = " + canvas);
                        synchronized (mHolder) {
                            isNeedRedraw = false;
                            List<IEntity> entities = mGraphModel.getEntities();
                            for (IEntity e : entities) {
                                e.draw(canvas);
                            }
                            Logging.w(" redraw()");
                        }
                    } finally {
                        if (canvas != null) {
                            mHolder.unlockCanvasAndPost(canvas);
                        }
                    }
                }
            }
        }

        @Override
        public synchronized void start() {
            isRunning = true;
            super.start();
        }

        @Override
        public void interrupt() {
            Logging.w("RenderThread :: interrupt");
            super.interrupt();
            isRunning = false;
        }

        public void update() {
            isNeedRedraw = true;
        }

        public void cancel() {
            isRunning = false;
            isNeedRedraw = false;
        }
    }

    @Override
    public void setOnTouchListener(com.example.graphsample.render.OnTouchListener onTouchListener) {
        mTouchListener = onTouchListener;
    }

    @Override
    public void update() {
        mRenderThread.update();
    }

    @Override
    public void setModel(IModel graphModel) {
        mGraphModel = graphModel;
    }

    @Override
    public void setRenderLifeCycleListener(IRenderLifeCycleListener lifeCycleListener) {
        mLifeCycleListener = lifeCycleListener;
    }

    @Override
    public void release() {
        mRenderThread.interrupt();
        mRenderThread = null;
        mLifeCycleListener = null;
        mTouchListener = null;
    }
}
