package com.example.graphsample.render;

import android.view.MotionEvent;

public interface OnTouchListener {

    boolean onTouch(MotionEvent event);

}
