package com.example.graphsample.render;

import com.example.graphsample.model.IModel;

public interface IRender {

    public interface IRenderLifeCycleListener {

        void onSurfaceCreated();

        void onSurfaceDestroyed();
    }

    void setOnTouchListener(OnTouchListener onTouchListener);

    void update();

    void setModel(IModel graphModel);

    void setRenderLifeCycleListener(IRenderLifeCycleListener lifeCycleListener);

    void release();

}
