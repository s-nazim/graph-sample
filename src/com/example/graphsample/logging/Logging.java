package com.example.graphsample.logging;

import android.util.Log;

public class Logging {

    private static final boolean ENABLED = true;
    private static final String TAG = "1";

    /**
     * See {@link #w(String, String)}
     */
    public static void w(String msg) {
        print(null, msg);
    }

    /**
     * See {@link Log#w(String, String)}
     */
    public static void w(String tag, String msg) {
        print(tag, msg);
    }

    private static void print(String tag, String msg) {
        if (tag == null) {
            tag = TAG;
        }
        if (Logging.ENABLED) {
            Log.w(tag, msg);
        }
    }
}
