package com.example.graphsample.graph;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class Edge implements IEntity {

    public final float startX;
    public final float startY;
    public final float endX;
    public final float endY;
    public final Vertex vertexFirst;
    public final Vertex vertexSecond;
    private Paint mPaint;

    public Edge(Vertex first, Vertex second) {
        this.startX = first.x;
        this.startY = first.y;
        this.endX = second.x;
        this.endY = second.y;
        this.vertexFirst = first;
        this.vertexSecond = second;

        mPaint = new Paint();
        mPaint.setColor(Color.BLUE);
        mPaint.setStrokeWidth(10);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawLine(startX, startY, endX, endY, mPaint);
    }

}
