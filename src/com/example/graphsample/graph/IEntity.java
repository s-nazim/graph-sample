package com.example.graphsample.graph;

import android.graphics.Canvas;

public interface IEntity {

    void draw(Canvas canvas);
}
