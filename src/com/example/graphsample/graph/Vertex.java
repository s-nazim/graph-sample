package com.example.graphsample.graph;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class Vertex implements IEntity {

    public final float x;
    public final float y;
    public final float radius;
    public int number;
    private Paint mPaint;

    public Vertex(float x, float y, float radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;

        mPaint = new Paint();
        mPaint.setColor(Color.GREEN);
        mPaint.setStrokeWidth(10);
    }

    public Vertex(float x, float y) {
        this(x, y, 0);
    }

    public Vertex(float x, float y, float radius, int number) {
        this(x, y, radius);
        this.number = number;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawCircle(x, y, radius, mPaint);
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setTextSize(radius * 1.5f);
        String numberStr = String.valueOf(number);
        canvas.drawText(numberStr, x - paint.measureText(numberStr) / 2, y + radius / 2, paint);
    }

    public void setColor(int red) {
        mPaint.setColor(red);
    }

}
