package com.example.graphsample;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

import com.example.graphsample.controller.GraphController;
import com.example.graphsample.controller.IController;
import com.example.graphsample.logging.Logging;
import com.example.graphsample.render.CustomSurfaceView;
import com.example.graphsample.render.IRender;
import com.example.graphsample.render.IRender.IRenderLifeCycleListener;
import com.example.roadsample.R;

public class WelcomeScreenActivity extends Activity {

    private IController mController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_welcome_screen);
        final IRender render = (CustomSurfaceView) findViewById(R.id.custom_surface_view);
        render.setRenderLifeCycleListener(new IRenderLifeCycleListener() {

            @Override
            public void onSurfaceDestroyed() {
                Logging.w("onSurfaceDestroyed() ");
            }

            @Override
            public void onSurfaceCreated() {
                Logging.w(" surfaceCreated() ");
                mController = new GraphController(render);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.welcome_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_dfs:
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mController.switchToDfsMode();
                }
            }, 500);
            return true;
        case R.id.action_settings:
            return true;
        case R.id.action_clear:
            mController.clear();
            return true;
        default:
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Logging.w(" onBackPressed() ");
        if (mController != null) {
            mController.release();
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        Logging.w(" onPause() ");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Logging.w(" onStop() ");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Logging.w(" onDestroy() ");
        super.onDestroy();
    }
}
